MOD_DIR=modules/texts/ztext/czecep
MODULES=$(MOD_DIR)/nt $(MOD_DIR)/nt.vss $(MOD_DIR)/ot $(MOD_DIR)/ot.vss
#OSIS2MOD=/home/matej/archiv/knihovna/repos/sword/utilities/osis2mod
OSIS2MOD=osis2mod

all: zip2crosswire

$(MODULES): CzeCEP.xml
	mkdir -p $(MOD_DIR)
	$(OSIS2MOD) $(MOD_DIR)  $< -v German -d 2

module: $(MODULES) czecep.conf
	install -D -m 644 czecep.conf mods.d/czecep.conf
	zip -9vT CzeCEP.zip $(MOD_DIR)/* mods.d/*

zip2crosswire: $(MODULES) czecep.conf
	zip -9vT CzeCEP.zip CzeCEP.xml czecep.conf

# CzeCEP.xml: CzeCEP.imp
# 	./imp2osis.pl CzeCEP CzeCEP.imp -o CzeCEP.xml -m

clean:
	rm -f *~ $(MODULES) CzeCEP.zip
